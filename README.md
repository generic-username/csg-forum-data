# CraftSurvivalGames Scraped Forum Data
Since enjin is shutting down, here is all the json data scraped from the forums.
- GetCategoryAndForums.json: All the forum category/subforum names
- forums: All the forums by id
- threads: All the threads by id
- users: all users
- images: all images (lookup URL in imglookup.json)

## Layout
data contains all the json data

## TODO
- static site generator for the data


# pages to make
 - main index page with link to text files/others
 - Forum category list
   - Forum Posts
     - individual posts
 - user list