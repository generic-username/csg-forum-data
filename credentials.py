
def get_credentials():
    with open(".credentials") as f:
        usr = f.readline()[:-1]
        pas = f.readline()
        return usr,pas

def get_login_obj():
    creds = get_credentials()
    return {
        "jsonrpc": "2.0",
        "method": "User.login",
        "params": {
            "email": creds[0],
            "password": creds[1]
        }
    }