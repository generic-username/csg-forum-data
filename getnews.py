import json

import requests
import os

from credentials import get_login_obj

url = 'http://www.craftsurvivalgames.com/api/v1/api.php'
session_id = "0j11inf3mdhqjjmffhg1jv3jn5"
s = requests.Session()
if session_id is None:
    myobj = get_login_obj()

    login_response = s.post(url=url, json=myobj)
    print("login: " + login_response.text)
    session_id = login_response.cookies.get_dict().get('api_auth')

print("sid:" + session_id)

myobj2 = {
    "jsonrpc": "2.0",
    "id": "1234567",
    "params": {
        "session_id": session_id,
        "preset_id": "12571632",
        "article_id": "4231643",
    },
    "method": "News.getArticle"
}
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
}
getcaf = s.post(url=url, json=myobj2, headers=headers)
wtf = getcaf.cookies.get_dict()
text = getcaf.text
js = getcaf.json()
print(text)
