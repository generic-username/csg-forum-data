import datetime
import json
from os import listdir
from os.path import isfile, join
import bbcode

from jinja2 import Environment, FileSystemLoader

environment = Environment(loader=FileSystemLoader("templates/"))
template = environment.get_template("thread.html")

threads = []
mypath = "data/threads"
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
# onlyfiles = ["9728851.json"]
for fname in onlyfiles:
    posts = []
    threadname = ""
    threadid = 0
    post_time = 0
    poster = None
    with open("data/threads/" + fname, "r") as f:
        for line in f:
            if line.startswith("{\"status\":\"error\","):
                continue
            obj = json.loads(line)
            if "error" in obj:
                continue
            for post in obj["result"]["posts"]:
                p_name = post["post_username"]
                if poster is None:
                    poster = p_name
                p_content = bbcode.render_html(post["post_content"])#.replace("\r", "\n<br>")
                p_time = datetime.datetime.fromtimestamp(int(post["post_time"]))
                posts.append([p_name, p_content, p_time])
            threadname = obj["result"]["thread"]["thread_subject"]
            threadid = obj["result"]["thread"]["thread_id"]
            post_time = obj["result"]["thread"]["thread_post_time"]
    threads.append([threadname, threadid, poster, datetime.datetime.fromtimestamp(int(post_time))])

    content = template.render(
        posts=posts, threadname=threadname
    )
    with open("data/static/" + fname.replace(".json", ".html"), "w", encoding="utf-8") as f:
        f.write(content)

index_template = environment.get_template("index.html")
index = index_template.render(
    threads=threads
)
with open("data/static/index.html", "w", encoding="utf-8") as f:
    f.write(index)
