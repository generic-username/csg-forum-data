import json
from time import sleep

import requests
import os
from os import listdir
from os.path import isfile, join

from credentials import get_login_obj

url = 'http://www.craftsurvivalgames.com/api/v1/api.php'
session_id = "0j11inf3mdhqjjmffhg1jv3jn5"
s = requests.Session()
if session_id is None:

    login_response = s.post(url=url, json=get_login_obj())
    print("login: " + login_response.text)
    session_id = login_response.cookies.get_dict().get('api_auth')

print("sid:" + session_id)

f = open('data/AllThreadIds.json')
jsdata = json.load(f)

thread_ids = []
for t_id in jsdata:
    thread_ids.append(t_id)


mypath = "data/threads"
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
for file in onlyfiles:
    tid = file.replace(".json", "")
    thread_ids.remove(tid)

size = len(thread_ids)
ran = 0
for t_id in thread_ids:
    ran += 1
    print("[%d/%d] dl thread: %s" % (ran, size, str(t_id)))
    page_no = 0
    while True:
        page_no += 1
        filename = "data/threads/" + str(t_id) + ".json"
        if page_no == 1 and os.path.isfile(filename):
            break
        myobj2 = {
            "jsonrpc": "2.0",
            "id": "1234567",
            "params": {
                "session_id": session_id,
                "thread_id": str(t_id),
                "page": str(page_no),
            },
            "method": "Forum.getThread"
        }
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
        }
        getcaf = s.post(url=url, json=myobj2, headers=headers)
        if getcaf.status_code == 405:
            print("Failed to dl: " + str(t_id))
            break
        wtf = getcaf.cookies.get_dict()
        text = getcaf.text
        js = getcaf.json()

        if "result" in js:
            page_count = js["result"]["pages"]

        # Write to file

        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "a") as f:
            f.write(text)
            f.write("\n")
        print("done page %d/%d" % (page_no, page_count))
        if page_count == page_no:
            break
