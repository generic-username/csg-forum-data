import json
import re

import requests
import os

from credentials import get_login_obj

url = 'http://www.craftsurvivalgames.com/api/v1/api.php'
session_id = "0j11inf3mdhqjjmffhg1jv3jn5"
s = requests.Session()
if session_id is None:
    myobj = get_login_obj()

    login_response = s.post(url=url, json=myobj)
    print("login: " + login_response.text)
    session_id = login_response.cookies.get_dict().get('api_auth')

user_ids = set()
print("sid:" + session_id)
files = []
for r, d, f in os.walk("data"):
    for file in f:
        if '.json' in file:
            files.append(os.path.join(r, file))
print("Running")
links = set()
ran = 0
for fname in files:
    f = open(fname, "r")
    contents = f.read()

    pos = 0
    while True:
        png_place = contents.find(".png", pos)
        jpg_place = contents.find(".jpg", pos)
        jpeg_place = contents.find(".jpeg", pos)
        extension_length = len(".png")

        if min(png_place, jpeg_place, jpg_place) == png_place:
            png_place = png_place
        elif min(png_place, jpeg_place, jpg_place) == jpg_place:
            png_place = jpg_place
        elif min(png_place, jpeg_place, jpg_place) == jpeg_place:
            png_place = jpeg_place
            extension_length = 5

        if png_place == -1:
            break
        https_place = contents.rfind("https:\\/\\/", 0, png_place)
        http_place = contents.rfind("http:\\/\\/", 0, png_place)
        quote_place = contents.rfind("\"", 0, png_place)
        pos = png_place + 4
        if quote_place > http_place and quote_place > https_place:
            # Invalid url, likely relative path
            continue
        # Use the one closer to the right
        if http_place != -1 and https_place != -1:
            img_link = contents[max(http_place, https_place):png_place + extension_length]
        elif https_place != -1:
            img_link = contents[https_place:png_place + extension_length]
        elif http_place != -1:
            img_link = contents[http_place:png_place + extension_length]
        else:
            raise Exception("No matching http_:// for image")

        links.add(img_link)

    f.close()

links = list(links)
links.sort()
n_links = {}
n = 0
for link in links:
    n += 1
    url = link.replace("\\", "")
    if url == "https://www.seattletimes.com/art/mlk/index.jpg"\
            or url == "http://seattletimes.com/art/mlk/index.jpg":
        continue
    n_links[n] = url
print(n_links)
for iid, url in n_links.items():
    try:
        print("[%d/%d] Getting: %s" % (iid, len(n_links.values()), url))
        img_data = requests.get(url).content
        ext = url[-4:]
        if ext == "jpeg":
            ext = ".jpeg"
        name = str(iid) + ext
        with open("data/images/" + name, 'wb') as handler:
            handler.write(img_data)
        print("Wrote " + name)
    except:
        print("Failed to download: " + url)

with open("data/imglookup.json", "w") as outfile:
    json.dump(n_links, outfile)
