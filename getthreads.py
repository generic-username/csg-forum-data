import json

import requests
import os

from credentials import get_login_obj

url = 'http://www.craftsurvivalgames.com/api/v1/api.php'
session_id = "0j11inf3mdhqjjmffhg1jv3jn5"
s = requests.Session()
if session_id is None:
    myobj = get_login_obj()

    login_response = s.post(url=url, json=myobj)
    print("login: " + login_response.text)
    session_id = login_response.cookies.get_dict().get('api_auth')

print("sid:" + session_id)

forum_ids = [
    #2419073 # Ban appeals, has announcement, sticky, threads
    4521166, 4521293, 2419073, 2419079, 2419081, 2419080, 4987271, 4174565, 2825348, 2419072, 3433563, 4145307, 2419048,
    4414876, 4414856, 2481335, 4414924, 4414935, 4414878, 4414933, 4414862, 4414904, 4414908, 4414886, 3270392, 2419050,
    2419051, 3476299, 3388267, 3421425, 2419049, 4226356, 3388812, 3388707, 3404757, 3388801
]
thread_list = []
for forum_id in forum_ids:
    not_done_pages = True
    page_no = 0
    while not_done_pages:
        page_no += 1
        myobj2 = {
            "jsonrpc": "2.0",
            "id": "1234567",
            "params": {
                "session_id": session_id,
                "forum_id": str(forum_id),
                "page": str(page_no),
            },
            "method": "Forum.getForum"
        }
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
        }
        getcaf = s.post(url=url, json=myobj2, headers=headers)
        wtf = getcaf.cookies.get_dict()
        text = getcaf.text
        js = getcaf.json()
        page_count = int(js["result"]["pages"])
        page_no = int(js["result"]["page"])

        # Construct the thread list
        for thread in js["result"]["announcement_global"]:
            thread_list.append(thread["thread_id"])
        for thread in js["result"]["sticky"]:
            thread_list.append(thread["thread_id"])
        for thread in js["result"]["threads"]:
            thread_list.append(thread["thread_id"])

        # Write to file
        filename = "data/forums/"+str(forum_id)+"/page"+str(page_no)+".json"
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "w") as f:
            f.write(text)
        print("finished page: %d/%d" % (page_no, page_count))
        if page_no == page_count:
            not_done_pages = False

    print("done forum")

with open("data/AllThreadIds.json", "w") as f:
    json.dump(thread_list, f)
