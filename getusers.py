import json

import requests
import os

from credentials import get_login_obj

url = 'http://www.craftsurvivalgames.com/api/v1/api.php'
session_id = "0j11inf3mdhqjjmffhg1jv3jn5"
s = requests.Session()
if session_id is None:
    myobj = get_login_obj()

    login_response = s.post(url=url, json=myobj)
    print("login: " + login_response.text)
    session_id = login_response.cookies.get_dict().get('api_auth')

user_ids = set()
print("sid:" + session_id)
scrape_for_users = True
if scrape_for_users:
    for file in os.listdir("data/threads"):
        f = open("data/threads/" + file, "r")
        for line in f:
            js = json.loads(line)
            # some json files are empty
            if "result" in js:
                op_id = js["result"]["thread"]["thread_user_id"]
                user_ids.add(op_id)
                for post in js["result"]["posts"]:
                    user_ids.add(post["post_user_id"])
print("Running")
ran = 0
for uid in user_ids:
    ran += 1
    print("Getting user: %s, (%d/%d)" % (uid, ran, len(user_ids)))
    myobj2 = {
        "jsonrpc": "2.0",
        "id": "1234567",
        "params": {
            "session_id": session_id,
            "user_id": str(uid),
            "limit": str(30),
        },
        # "method": "Profile.getWall"
        # "method": "Profile.getPhotos"
        # Profile.getFullInfo
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
    }
    myobj2["method"] = "Profile.getWall"
    wall = s.post(url=url, json=myobj2, headers=headers)

    myobj2["method"] = "Profile.getPhotos"
    photos = s.post(url=url, json=myobj2, headers=headers)

    myobj2["method"] = "Profile.getFullInfo"
    fullinfo = s.post(url=url, json=myobj2, headers=headers)
    f = open("data/users/" + str(uid) + ".json", "w")
    f.write(wall.text + "\n")
    f.write(photos.text + "\n")
    f.write(fullinfo.text)
    f.close()

